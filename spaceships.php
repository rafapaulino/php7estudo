<?php

$games = ['Super Mario','Asphaut 8','123 Chapolin Pelado','Nariz de 12','Campeonato Brasileiro'];

usort($games, function($a,$b) {

	var_dump('a = '.$a.', b = '.$b);

	return $a <=> $b; // -1,0,1

});

var_dump($games);


class User {

	protected $name;
	protected $age;

	/**
	 * Class Constructor
	 * @param    $name   
	 * @param    $age   
	 */
	public function __construct($name, $age)
	{
		$this->name = $name;
		$this->age = $age;
	}

	/**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of age.
     *
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }
}

class UserCollection {

	protected $users;

	public function __construct(array $users)
	{
		$this->users = $users;
	}

	public function sortBy($property)
	{
		usort($this->users, function($userOne, $userTwo) use ($property) {

			return $userOne->$property() <=> $userTwo->$property(); // -1,0,1

		});
	}

	public function getUsers()
	{
		return $this->users;
	}
}


$collection = new UserCollection([
	new User('João',18),
	new User('Maria',22),
	new User('Juliana',23)
]);

$collection->sortBy('getName');



var_dump($collection->getUsers());
