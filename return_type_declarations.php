<?php

class User {}

interface SomeInterface {

	public function getUser() : User;
}

class Ninja implements SomeInterface {

	public function getUser() : User
	{
		return new User;
	}
}

function getUser() : User {

	return new User;
}

var_dump(getUser());


(new Ninja)->getUser();
